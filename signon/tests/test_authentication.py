from __future__ import annotations
from unittest import mock
from django.test import TestCase, override_settings
from django.urls import reverse
from django.contrib.auth import get_user_model
from signon.unittest import SignonFixtureMixin
from signon import providers


@override_settings(SIGNON_PROVIDERS=[
    providers.DebssoProvider(name="debsso", label="sso.debian.org"),
    providers.Provider(name="salsa", label="Salsa", single_bind=True),
])
class TestAuthentication(SignonFixtureMixin, TestCase):
    def test_no_active_identities(self):
        client = self.make_test_client(None)
        response = client.get(reverse('signon:whoami'))
        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {})
        self.assertFalse(request.user.is_authenticated)

    def test_one_active_unbound_identity(self):
        self.identities.create("user", issuer="debsso", subject="user@debian.org", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.user,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.user,
        })
        self.assertFalse(request.user.is_authenticated)

    def test_one_active_bound_identity(self):
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user@debian.org", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.user1,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.user1,
        })
        self.assertEqual(request.user, self.user1)

    def test_conflicting_active_bound_identities(self):
        # Multiple active bound identities pointing to different people cause
        # failure to authenticate
        self.identities.create("user1", person=self.user1, issuer="debsso", subject="user1@debian.org", audit_skip=True)
        self.identities.create("user2", person=self.user2, issuer="salsa", subject="2", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.user1,
                "salsa": self.identities.user2,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon:whoami'))

        self.assertEqual(log.output, [
            'ERROR:signon.middleware:Conflicting person mapping: identities '
            f'({self.user1}:debsso:user1@debian.org, {self.user2}:salsa:2) map to at least'
            f' {self.user1} and {self.user2}'
        ])

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.user1,
            "salsa": self.identities.user2,
        })
        self.assertFalse(request.user.is_authenticated)

    def test_aligned_active_bound_identities(self):
        # Multiple active bound identities pointing to different people cause
        # failure to authenticate
        self.identities.create(
                "debsso", person=self.user1, issuer="debsso", subject="user1@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", person=self.user1, issuer="salsa", subject="1", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
            "salsa": self.identities.salsa,
        })
        self.assertEqual(request.user, self.user1)

    def test_remove_user_on_identity_deactivated(self):
        client = self.make_test_client(None)

        self.identities.create(
                "salsa", person=self.user1, issuer="salsa", subject="1", audit_skip=True)

        # Authenticate successfully with one active identity

        def _instantiate_identities1(_self, request):
            request.signon_identities = {
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities1):
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.user, self.user1)

        # Deactivate identities

        def _instantiate_identities2(_self, request):
            request.signon_identities = {}

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities2):
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertFalse(request.user.is_authenticated)

    def test_remove_user_on_identity_conflict(self):
        client = self.make_test_client(None)

        self.identities.create(
                "debsso", person=self.user1, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", person=self.user2, issuer="salsa", subject="2", audit_skip=True)

        # Authenticate successfully with one active identity

        def _instantiate_identities1(_self, request):
            request.signon_identities = {
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities1):
            response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.user, self.user2)

        # Add another conflicting identity, authentication should disappear

        def _instantiate_identities2(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities2):
            with self.assertLogs():
                response = client.get(reverse('signon:whoami'))

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertFalse(request.user.is_authenticated)

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_auto_bind(self):
        self.identities.create(
                "debsso", person=self.user1, issuer="debsso", subject="dd@debian.org", audit_skip=True)
        self.identities.create(
                "salsa", issuer="salsa", subject="1", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon:whoami'))

        self.assertEqual(log.output, [
            f"INFO:signon.middleware:{self.user1}: auto associated to -:salsa:1",
        ])

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
            "salsa": self.identities.salsa,
        })
        self.assertEqual(request.user, self.user1)

        self.identities.debsso.refresh_from_db()
        self.assertEqual(self.identities.debsso.person, self.user1)

        self.identities.salsa.refresh_from_db()
        self.assertEqual(self.identities.salsa.person, self.user1)

    @override_settings(SIGNON_AUTO_BIND=True)
    def test_multiple_salsa_accounts(self):
        # One bound debsso account
        self.identities.create(
                "debsso", person=self.user1, issuer="debsso", subject="user1@debian.org", audit_skip=True)
        # One bound salsa account
        self.identities.create(
                "salsa1", person=self.user1, issuer="salsa", subject="1", audit_skip=True)
        # One unbound salsa account
        self.identities.create(
                "salsa2", issuer="salsa", subject="2", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
                "salsa": self.identities.salsa2,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon:whoami'))

        self.assertEqual(log.output, [
            f"INFO:signon.middleware:{self.user1}:"
            f" skipping association to -:salsa:2 because {self.user1}:salsa:1 is already associated",
            f'INFO:signon.middleware:{self.user1}: logging out spurious identity -:salsa:2',
        ])

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
        })
        self.assertEqual(request.user, self.user1)

        self.identities.debsso.refresh_from_db()
        self.assertEqual(self.identities.debsso.person, self.user1)

        self.identities.salsa1.refresh_from_db()
        self.assertEqual(self.identities.salsa1.person, self.user1)

        self.identities.salsa2.refresh_from_db()
        self.assertIsNone(self.identities.salsa2.person)

    @override_settings(SIGNON_AUTO_CREATE_USER=True)
    def test_auto_create_user_debsso(self):
        User = get_user_model()
        if not hasattr(User.objects, "create_from_identity"):
            self.skipTest("user model manager does not implement create_from_identity")

        # One bound debsso account
        self.identities.create(
                "debsso", issuer="debsso", subject="new@debian.org", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "debsso": self.identities.debsso,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon:whoami'))

        self.identities.debsso.refresh_from_db()
        person = self.identities.debsso.person

        self.assertEqual(log.output, [
            f"INFO:signon.middleware:{person}: auto created from identity -:debsso:new@debian.org",
            f"INFO:signon.middleware:{person}: auto bound to identity {person}:debsso:new@debian.org",
        ])

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "debsso": self.identities.debsso,
        })

        self.assertIsNotNone(person)
        self.assertEqual(list(person.identities.all()), [self.identities.debsso])

    @override_settings(SIGNON_AUTO_CREATE_USER=True)
    def test_auto_create_user_salsa(self):
        User = get_user_model()
        if not hasattr(User.objects, "create_from_identity"):
            self.skipTest("user model manager does not implement create_from_identity")

        # One bound debsso account
        self.identities.create(
                "salsa", issuer="salsa", subject="2", username="new", audit_skip=True)

        def _instantiate_identities(_self, request):
            request.signon_identities = {
                "salsa": self.identities.salsa,
            }

        with mock.patch("signon.middleware.SignonMiddleware._instantiate_identities", _instantiate_identities):
            client = self.make_test_client(None)
            with self.assertLogs() as log:
                response = client.get(reverse('signon:whoami'))

        self.identities.salsa.refresh_from_db()
        person = self.identities.salsa.person

        self.assertEqual(log.output, [
            f"INFO:signon.middleware:{person}: auto created from identity -:salsa:2",
            f"INFO:signon.middleware:{person}: auto bound to identity {person}:salsa:2",
        ])

        self.assertEqual(response.status_code, 200)

        request = response.wsgi_request
        self.assertEqual(request.signon_identities, {
            "salsa": self.identities.salsa,
        })

        self.assertIsNotNone(person)
        self.assertEqual(list(person.identities.all()), [self.identities.salsa])
