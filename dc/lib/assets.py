# Copyright © 2020 Truelite S.r.l
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from typing import Iterable
from django.contrib.staticfiles.storage import staticfiles_storage
from django.conf import settings
import os


class Asset:
    """
    Information passed to templates about one asset
    """
    __slots__ = ("type", "path")

    def __init__(self, type, path, prefix=None):
        # Asset type (js, css, other)
        self.type = type
        if prefix is None:
            self.path = str(path)
        else:
            self.path = os.path.join(prefix, str(path))

    @property
    def uri(self):
        """
        URI used to link to the asset in a normal django template
        """
        return staticfiles_storage.url(str(self.path))

    @property
    def mobile_uri(self):
        """
        URI used to link to the asset in mobile applications with service
        workers, which need static assets exported alongside the other mobile
        data
        """
        mobile_static_url = getattr(settings, "MOBILE_STATIC_URL", None)
        if mobile_static_url is None:
            return self.uri
        else:
            return os.path.join(mobile_static_url, str(self.path))


class Assets:
    """
    Describe a bundle of assets, with optional dependencies.

    Create subclasses of Assets to describe a bundle of assets that a view may
    want to use (like JQuery, Bootstrap4, and so on).
    """
    # Tuple of asset bundles that are dependencies of this one
    assets = ()  # type: Tuple[Assets]

    # Prefix for all assets listed under STATIC_URL or MOBILE_STATIC_URL
    # The special prefix common points to /usr/share/javascript
    prefix = None

    # Paths to JavaScript assets (relative to prefix)
    js = ()

    # Paths to CSS assets (relative to prefix)
    css = ()

    # Paths to other kinds of assets (relative to prefix)
    other = ()

    @classmethod
    def get_asset(cls, type, relpath):
        """
        Make an Asset entry
        """
        return Asset(type, relpath, prefix=cls.prefix)

    @classmethod
    def all_assets(cls) -> Iterable["Assets"]:
        """
        Generate all asset dependencies and then this asset class, in order,
        removing duplicates.
        """
        seen = set()
        for dep in cls.assets:
            for a in dep.all_assets():
                if a in seen:
                    continue
                seen.add(a)
                yield a
        if cls not in seen:
            yield cls

    @classmethod
    def all_js(cls) -> Iterable[Asset]:
        """
        Generate Asset entries for all JavaScript assets in dependencies and
        this class, in order, without duplicates
        """
        seen = set()
        for assets in cls.all_assets():
            for asset in assets.js:
                a = assets.get_asset("js", asset)
                if a.path in seen:
                    continue
                seen.add(a.path)
                yield a

    @classmethod
    def all_css(cls) -> Iterable[Asset]:
        """
        Generate Asset entries for all JavaScript assets in dependencies and
        this class, in order, without duplicates
        """
        seen = set()
        for assets in cls.all_assets():
            for asset in assets.css:
                a = assets.get_asset("css", asset)
                if a.path in seen:
                    continue
                seen.add(a.path)
                yield a

    @classmethod
    def all_other(cls) -> Iterable[Asset]:
        """
        Generate Asset entries for all JavaScript assets in dependencies and
        this class, in order, without duplicates
        """
        seen = set()
        for assets in cls.all_assets():
            for asset in assets.other:
                a = assets.get_asset("other", asset)
                if a.path in seen:
                    continue
                seen.add(a.path)
                yield a

    @classmethod
    def all(cls) -> Iterable[Asset]:
        """
        Generate Asset entries for all assets in dependencies and in this
        class, in order, without duplicates
        """
        yield from cls.all_css()
        yield from cls.all_js()
        yield from cls.all_other()


class Empty(Assets):
    """
    Empty asset bundle
    """
    pass


def bundle(*assets):
    """
    Create an Assets grouping various other Assets together
    """
    filtered = tuple(a for a in assets if a is not Empty)
    if not filtered:
        return Empty
    return type("AssetBundle", (Assets,), {"assets": filtered})


class AssetMixin:
    """
    Mixin used to configure assets for Django views and make them available to
    templates.

    In templates, use assets like this::

       {% if assets %}
       {% for a in assets.all_css %}<link rel="stylesheet" href="{{a.uri}}" />
       {% endfor %}
       {% for a in assets.all_js %}<script src="{{a.uri}}"></script>
       {% endfor %}
       {% endif %}

    In views, define the ``assets`` class method to an Assets subclass or a
    list or tuple of Assets subclasses.

    The assets passed to templates will be a merge, without duplicates and in
    depedency order, of all assets defined by the view and all its base
    classes.
    """
    # Default to no assets
    assets = Empty

    def get_assets(self):
        """
        Get the assets for this view, by bundling together all the assets
        members of parent classes.

        You probably do not need to override this. If you do, you can do it
        as::

            def get_assets(self):
                this_assets = my_custom_assets_function()
                return bundle(super().get_assets(), *this_assets)
        """
        assets = []
        for cls in self.__class__.__mro__:
            attr = getattr(cls, 'assets', None)
            if attr is None:
                continue
            elif isinstance(attr, type) and issubclass(attr, Assets):
                assets.append(attr)
            else:
                assets.extend(attr)

        return bundle(*assets)

    def get_context_data(self, **kw):
        """
        Include ``assets`` in the view context
        """
        ctx = super().get_context_data(**kw)
        ctx["assets"] = self.get_assets()
        return ctx


#
# Define well-known standard bundles of assets
#

class JQuery(Assets):
    # libjs-jquery (stretch, buster)
    prefix = 'common/jquery'
    js = ['jquery.min.js']


class PopperJS(Assets):
    # libjs-popper.js (buster)
    prefix = "common/popper.js"
    js = ['umd/popper.min.js']


class Bootstrap3(Assets):
    # libjs-bootstrap (stretch, buster)
    assets = [JQuery]
    prefix = 'common/bootstrap'
    css = ['css/bootstrap.min.css']
    js = ['js/bootstrap.min.js']
    other = [
        "fonts/glyphicons-halflings-regular.eot",
        "fonts/glyphicons-halflings-regular.svg",
        "fonts/glyphicons-halflings-regular.ttf",
        "fonts/glyphicons-halflings-regular.woff",
        "fonts/glyphicons-halflings-regular.woff2",
    ]


class Bootstrap4(Assets):
    # libjs-bootstrap4 (buster)
    assets = [JQuery, PopperJS]
    prefix = 'common/bootstrap4'
    css = ['css/bootstrap.min.css']
    js = ['js/bootstrap.min.js']


class DataTablesBase(Assets):
    # libjs-jquery-datatables (stretch, buster)
    assets = [JQuery]
    prefix = 'common/jquery-datatables'
    js = ['jquery.dataTables.min.js']
    other = [
        'images/favicon.ico',
        'images/sort_asc_disabled.png',
        'images/sort_asc.png',
        'images/sort_both.png',
        'images/sort_desc_disabled.png',
        'images/sort_desc.png',
    ]


class DataTables(Assets):
    assets = [DataTablesBase]
    # This css is only required when not using bootstrap integration
    # see https://github.com/DataTables/Plugins/issues/39#issuecomment-35016269
    css = ['css/jquery.dataTables.min.css']


class DataTablesBootstrap3(Assets):
    # libjs-jquery-datatables (stretch, buster)
    assets = [Bootstrap3, DataTablesBase]
    prefix = 'common/jquery-datatables'
    css = ['css/dataTables.bootstrap.css']
    js = ['dataTables.bootstrap.min.js']


class DataTablesBootstrap4(Assets):
    # libjs-jquery-datatables (stretch, buster)
    assets = [Bootstrap4, DataTablesBase]
    prefix = 'common/jquery-datatables'
    css = ['css/dataTables.bootstrap4.css']
    js = ['dataTables.bootstrap4.min.js']


class DataTablesRowReorderBootstrap3(Assets):
    # libjs-jquery-datatables-extensions (obsolete in both stretch-backports and buster)
    assets = [DataTablesBootstrap3]
    prefix = "compat/RowReorder-1.2.4/"
    css = ['css/rowReorder.bootstrap.min.css']
    js = [
        'js/dataTables.rowReorder.min.js',
        'js/rowReorder.bootstrap.min.js',
    ]


class DataTablesRowReorderBootstrap4(Assets):
    # libjs-jquery-datatables-extensions (obsolete in both stretch-backports and buster)
    assets = [DataTablesBootstrap4]
    prefix = "compat/RowReorder-1.2.4/"
    css = ['css/rowReorder.bootstrap4.min.css']
    js = [
        'js/dataTables.rowReorder.min.js',
        'js/rowReorder.bootstrap4.min.js',
    ]


class Leaflet(Assets):
    # libjs-leaflet (stetch:obsolete, buster)
    prefix = 'common/leaflet'
    css = ['leaflet.css']
    js = ['leaflet.min.js']
    other = [
        'images/layers-2x.png',
        'images/layers.png',
        'images/marker-icon-2x.png',
        'images/marker-icon.png',
        'images/marker-shadow.png',
    ]


class LeafletMarkerCluster(Assets):
    # libjs-leaflet.markercluster (buster)
    assets = [Leaflet]
    prefix = 'common/leaflet'
    css = ['MarkerCluster.css', 'MarkerCluster.Default.css']
    js = ['leaflet.markercluster.min.js']


class ForkAwesome(Assets):
    # fonts-fork-awesome (buster)
    # fonts-font-awesome would exist in stretch, but does not ship the
    # /usr/share/javascript symlink, so we need to bundle anyway
    prefix = 'common/fork-awesome'
    css = [
        'css/fork-awesome.css',
    ]
    other = (
        'fonts/forkawesome-webfont.eot',
        'fonts/forkawesome-webfont.svg',
        'fonts/forkawesome-webfont.ttf',
        'fonts/forkawesome-webfont.woff',
        'fonts/forkawesome-webfont.woff2',
    )


class Mustache(Assets):
    # libjs-mustache (stretch, buster)
    prefix = 'common/mustache'
    js = [
        'mustache.min.js',
    ]


class Flatpickr(Assets):
    # flatpickr (not in Debian)
    # see compat/flatpickr/README.md
    prefix = "compat/flatpickr"
    css = [
        "flatpickr.min.css",
    ]
    js = [
        "flatpickr.min.js",
        "it.js",
    ]


class Nunjucks(Assets):
    # nunjucks (not in Debian)
    # see compat/nunjucks/README.md
    prefix = "compat/nunjucks"
    js = [
        "nunjucks.min.js",
    ]


class Select2(Assets):
    # select2 (not in Debian)
    # see compat/select2/README.md
    prefix = "compat/select2"
    js = [
        "js/select2.min.js",
    ]
    css = [
        "css/select2.min.css",
    ]


class Select2Bootstrap3(Assets):
    # select2-bootstrap (not in Debian)
    # see compat/select2-bootstrap/README.md
    assets = [Bootstrap3, Select2]
    prefix = "compat/select2-bootstrap"
    css = [
        "select2-bootstrap.min.css",
    ]


class Select2Bootstrap4(Assets):
    # select2-bootstrap4 (not in Debian)
    # see compat/select2-bootstrap4/README.md
    assets = [Bootstrap4, Select2]
    prefix = "common/select2-bootstrap4"
    css = [
        "select2-bootstrap4.min.css",
    ]


class Tablesorter(Assets):
    # libjs-jquery-tablesorter
    assets = [JQuery]
    prefix = "common/jquery-tablesorter"
    css = [
        "css/theme.bootstrap_4.css",
    ]
    js = [
        "jquery.tablesorter.min.js"
    ]


class Flot(Assets):
    # libjs-jquery-flot
    assets = [JQuery]
    prefix = "common/jquery-flot"
    js = [
        "jquery.flot.min.js",
        "jquery.flot.pie.min.js",
    ]
