from __future__ import annotations
import datetime
from django.test import TestCase
from django.urls import reverse
from contributors import models as cmodels
from dc.unittest import SourceFixtureMixin


class SourcesTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_public_views, user)

        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_source_add_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_source_add_forbidden, user)

        for user in "dd", "dd1", "alioth", "alioth1":
            cls._add_method(cls._test_source_update_success, user)
            cls._add_method(cls._test_source_update_forbidden, user)
            cls._add_method(cls._test_source_delete_success, user)
            cls._add_method(cls._test_source_delete_forbidden, user)
            cls._add_method(cls._test_source_delete_contributions_success, user)
            cls._add_method(cls._test_source_delete_contributions_forbidden, user)
        cls._add_method(cls._test_source_update_success, "admin", skip_admin=True)
        cls._add_method(cls._test_source_update_forbidden, None)
        cls._add_method(cls._test_source_delete_success, "admin", skip_admin=True)
        cls._add_method(cls._test_source_delete_forbidden, None)
        cls._add_method(cls._test_source_delete_contributions_success, "admin", skip_admin=True)
        cls._add_method(cls._test_source_delete_contributions_forbidden, None)

        cls._add_method(cls._test_backup_visible, "admin")
        cls._add_method(cls._test_backup_not_visible, "dd")
        cls._add_method(cls._test_backup_visible, "dd", member=True)
        cls._add_method(cls._test_backup_not_visible, "alioth")
        cls._add_method(cls._test_backup_visible, "alioth", member=True)
        cls._add_method(cls._test_backup_not_visible, None)

    def _test_public_views(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:list"))
        self.assertContains(response, "Debian Contributors data sources")

        response = client.get(reverse("sources:view", args=["test"]))
        self.assertContains(response, "'test' data source")

    def _test_source_add_success(self, user):
        client = self.make_test_client(user)
        self.assertEqual(cmodels.Source.objects.count(), 1)

        response = client.get(reverse("sources:add"))
        self.assertContains(response, "Create new Debian Contributors data source")

        response = client.post(reverse("sources:add"))
        self.assertContains(response, "Create new Debian Contributors data source")

        self.assertEqual(cmodels.Source.objects.count(), 1)

        response = client.post(reverse("sources:add"), data={
            "name": "newsource",
            "desc": "new test source",
            "url": "http://www.example.org/new",
            "auth_token": "newtoken",
        })
        self.assertRedirects(response, reverse("sources:list"))

        self.assertEqual(cmodels.Source.objects.count(), 2)
        s = cmodels.Source.objects.get(name="newsource")
        self.assertEqual(s.desc, "new test source")
        self.assertEqual(s.url, "http://www.example.org/new")
        self.assertEqual(s.auth_token, "newtoken")
        self.assertTrue(s.admins.filter(pk=self.users[user].pk).exists())

    def _test_source_add_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:add"))
        self.assertPermissionDenied(response)

        response = client.post(reverse("sources:add"))
        self.assertPermissionDenied(response)

        self.assertEqual(cmodels.Source.objects.count(), 1)

    def _test_source_update_success(self, user, skip_admin=False):
        client = self.make_test_client(user)
        if not skip_admin:
            self.sources.test.admins.add(self.users[user])

        response = client.get(reverse("sources:update", args=[self.sources.test.name]))
        self.assertContains(response, "Edit test data source")

        response = client.post(reverse("sources:update", args=[self.sources.test.name]))
        self.assertContains(response, "Edit test data source")

        response = client.post(reverse("sources:update", args=[self.sources.test.name]), data={
            "name": "test",
            "desc": "new test source",
            "url": "http://www.example.org/new",
            "auth_token": "newtestsecret",
        })
        self.assertRedirects(response, reverse("sources:list"))

        self.sources.refresh()
        s = self.sources.test
        self.assertEqual(s.desc, "new test source")
        self.assertEqual(s.url, "http://www.example.org/new")
        self.assertEqual(s.auth_token, "newtestsecret")

    def _test_source_update_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:update", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("sources:update", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

    def _test_source_delete_success(self, user, skip_admin=False):
        client = self.make_test_client(user)
        if not skip_admin:
            self.sources.test.admins.add(self.users[user])

        response = client.get(reverse("sources:delete", args=[self.sources.test.name]))
        self.assertContains(response, "Delete source test?")

        response = client.post(reverse("sources:delete", args=[self.sources.test.name]))
        self.assertRedirects(response, reverse("sources:list"))

        self.assertEqual(cmodels.Source.objects.count(), 0)

    def _test_source_delete_forbidden(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:delete", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("sources:delete", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

    def _recompute_aggregates(self, user):
        cmodels.AggregatedPersonContribution.recompute(ctype=self.ctypes.tester)
        cmodels.AggregatedSource.recompute(source=self.sources.test)
        cmodels.AggregatedPerson.recompute()

    def _test_source_delete_contributions_success(self, user, skip_admin=False):
        self._recompute_aggregates(user)
        if not skip_admin:
            self.sources.test.admins.add(self.users[user])

        client = self.make_test_client(user)
        response = client.get(reverse("sources:delete_contributions", args=[self.sources.test.name]))
        self.assertContains(response, "Delete contributions for source test")

        response = client.post(reverse("sources:delete_contributions", args=[self.sources.test.name]))
        self.assertRedirects(response, self.sources.test.get_absolute_url())

        self.assertEqual(self.ctypes.tester.contributions.count(), 0)
        self.assertEqual(cmodels.AggregatedSource.objects.count(), 0)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 0)
        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 0)

    def _test_source_delete_contributions_forbidden(self, user):
        self._recompute_aggregates(user)

        client = self.make_test_client(user)
        response = client.get(reverse("sources:delete_contributions", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

        response = client.post(reverse("sources:delete_contributions", args=[self.sources.test.name]))
        self.assertPermissionDenied(response)

        self.assertEqual(self.ctypes.tester.contributions.count(), 10)
        self.assertEqual(cmodels.AggregatedSource.objects.count(), 1)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.count(), 5)
        self.assertEqual(cmodels.AggregatedPerson.objects.count(), 5)

    def _test_backup_visible(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        client = self.make_test_client(user)
        response = client.get(reverse("sources:backup_list", kwargs={"sname": "test"}))
        self.assertEqual(response.status_code, 200)

        response = client.get(reverse("sources:backup_download", kwargs={"sname": "test", "backup_id": "20150208"}))
        self.assertEqual(response.status_code, 404)

    def _test_backup_not_visible(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:backup_list", kwargs={"sname": "test"}))
        self.assertPermissionDenied(response)

        response = client.get(reverse("sources:backup_download", kwargs={"sname": "test", "backup_id": "20150208"}))
        self.assertPermissionDenied(response)


class SourceMembersTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        cls._add_method(cls._test_source_members_visible, "admin")
        cls._add_method(cls._test_source_members_visible, "dd")
        cls._add_method(cls._test_source_members_notvisible, "alioth")
        cls._add_method(cls._test_source_members_notvisible, None)
        cls._add_method(cls._test_source_members_visible, "alioth", member=True)

        # Superuser can add anyone
        for user in "admin", "dd", "alioth":
            cls._add_method(cls._test_can_add, "admin", user)

        cls._add_method(cls._test_can_add, "dd", "dd")
        for user in "admin", "dd1", "alioth":
            # Non-member DD can only add self
            cls._add_method(cls._test_cannot_add, "dd", user)
            # Member DD can add anyone
            cls._add_method(cls._test_can_add, "dd", user, member=True)

        cls._add_method(cls._test_cannot_add, "alioth", "alioth")
        for user in "admin", "dd", "alioth1":
            # Non-member alioth cannot add anyone
            cls._add_method(cls._test_cannot_add, "alioth", user)
            # Member alioth can add anyone
            cls._add_method(cls._test_can_add, "alioth", user, member=True)

        # Anonymous cannot add anyone
        for user in "admin", "dd", "alioth":
            cls._add_method(cls._test_cannot_add, None, user)

        # Superuser can delete
        cls._add_method(cls._test_can_delete, "admin")

        # Nonmembers cannot delete
        cls._add_method(cls._test_cannot_delete, "dd")
        cls._add_method(cls._test_cannot_delete, "alioth")

        # Members can delete anyone
        cls._add_method(cls._test_can_delete, "dd", member=True)
        cls._add_method(cls._test_can_delete, "alioth", member=True)

        # Anonymous cannot delete anyone
        cls._add_method(cls._test_cannot_delete, None)

    def _test_source_members_visible(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])
        client = self.make_test_client(user)
        response = client.get(reverse("sources:members", args=["test"]))
        self.assertEqual(response.status_code, 200)

    def _test_source_members_notvisible(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:members", args=["test"]))
        self.assertPermissionDenied(response)

    def _test_can_add(self, user, added, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        client = self.make_test_client(user)

        response = client.get(reverse("sources:members_add", args=["test"]))
        self.assertEqual(response.status_code, 405)  # Method not allowed

        response = client.post(reverse("sources:members_add", args=["test"]))
        self.assertEqual(response.status_code, 404)  # Not found

        response = client.post(reverse("sources:members_add", args=["test"]), data={"name": "missing@example.org"})
        self.assertEqual(response.status_code, 404)  # Not found

        response = client.post(reverse("sources:members_add", args=["test"]), data={"name": self.users[added].username})
        self.assertRedirects(response, reverse("sources:members", args=["test"]))

        self.assertIn(self.users[added], list(self.sources.test.admins.all()))

    def _test_cannot_add(self, user, added):
        client = self.make_test_client(user)

        response = client.get(reverse("sources:members_add", args=["test"]))
        self.assertIn(response.status_code, [403, 405])  # Forbidden, Method not allowed

        response = client.post(reverse("sources:members_add", args=["test"]))
        self.assertIn(response.status_code, [403, 404])  # Forbidden, Not found

        response = client.post(reverse("sources:members_add", args=["test"]), data={"name": "missing@example.org"})
        self.assertIn(response.status_code, [403, 404])  # Forbidden, Not found

        response = client.post(reverse("sources:members_add", args=["test"]), data={"name": self.users[added].username})
        self.assertPermissionDenied(response)
        self.assertNotIn(self.users[added], list(self.sources.test.admins.all()))

    def _test_can_delete(self, user, member=False):
        self.sources.test.admins.add(self.users.dd1)

        if member:
            self.sources["test"].admins.add(self.users[user])

        client = self.make_test_client(user)

        response = client.get(reverse("sources:members_delete", args=["test"]))
        self.assertEqual(response.status_code, 405)  # Method not allowed

        response = client.post(reverse("sources:members_delete", args=["test"]))
        self.assertEqual(response.status_code, 404)  # Not found

        response = client.post(reverse("sources:members_delete", args=["test"]), data={"name": "missing@example.org"})
        self.assertEqual(response.status_code, 404)  # Not found

        response = client.post(reverse("sources:members_delete", args=["test"]), data={"name": self.users.dd1.username})
        self.assertRedirects(response, reverse("sources:members", args=["test"]))

        self.assertNotIn(self.users.dd1, list(self.sources.test.admins.all()))

    def _test_cannot_delete(self, user):
        self.sources.test.admins.add(self.users.dd1)

        client = self.make_test_client(user)

        response = client.get(reverse("sources:members_delete", args=["test"]))
        self.assertIn(response.status_code, [403, 405])  # Forbidden, Method not allowed

        response = client.post(reverse("sources:members_delete", args=["test"]))
        self.assertIn(response.status_code, [403, 404])  # Forbidden, Not found

        response = client.post(reverse("sources:members_delete", args=["test"]), data={"name": "missing@example.org"})
        self.assertIn(response.status_code, [403, 404])  # Forbidden, Not found

        response = client.post(reverse("sources:members_delete", args=["test"]), data={"name": self.users.dd.username})
        self.assertIn(response.status_code, [403, 404])  # Forbidden, Not found

        response = client.post(reverse("sources:members_delete", args=["test"]), data={"name": self.users.dd1.username})
        self.assertPermissionDenied(response)
        self.assertIn(self.users.dd1, list(self.sources.test.admins.all()))


class SourceCtypeTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Any user can view contribution types
        for user in "admin", "dd", "alioth", None:
            cls._add_method(cls._test_view_success, user)

        # Admins can add contribution types without being members
        cls._add_method(cls._test_can_add, "admin")
        # DD and alioth can add contribution types only if members
        cls._add_method(cls._test_cannot_add, "dd")
        cls._add_method(cls._test_can_add, "dd", member=True)
        cls._add_method(cls._test_cannot_add, "alioth")
        cls._add_method(cls._test_can_add, "alioth", member=True)
        # Anonymous cannot add contribution types
        cls._add_method(cls._test_cannot_add, None)

        # Admins can update contribution types without being members
        cls._add_method(cls._test_can_update, "admin")
        # DD and alioth can update contribution types only if members
        cls._add_method(cls._test_cannot_update, "dd")
        cls._add_method(cls._test_can_update, "dd", member=True)
        cls._add_method(cls._test_cannot_update, "alioth")
        cls._add_method(cls._test_can_update, "alioth", member=True)
        # Anonymous cannot update contribution types
        cls._add_method(cls._test_cannot_update, None)

        # Admins can delete contribution types without being members
        cls._add_method(cls._test_can_delete, "admin")
        # DD and alioth can delete contribution types only if members
        cls._add_method(cls._test_cannot_delete, "dd")
        cls._add_method(cls._test_can_delete, "dd", member=True)
        cls._add_method(cls._test_cannot_delete, "alioth")
        cls._add_method(cls._test_can_delete, "alioth", member=True)
        # Anonymous cannot delete contribution types
        cls._add_method(cls._test_cannot_delete, None)

    def _test_view_success(self, user):
        client = self.make_test_client(user)
        response = client.get(reverse("sources:ctype_view", kwargs={"sname": "test", "name": "tester"}))
        self.assertEqual(response.status_code, 200)

    def _test_can_add(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        url = reverse("sources:ctype_add", kwargs={"sname": "test"})

        client = self.make_test_client(user)

        # Get (show form)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        # Post (show form with validation errors)
        response = client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.sources.test.contribution_types.filter(name="pinger").exists())

        # Post (valid data)
        response = client.post(url, data={"name": "pinger", "desc": "Pinger", "contrib_desc": "Pinging"})
        self.assertRedirects(response, reverse("sources:update", args=["test"]))
        ct = self.sources.test.contribution_types.get(name="pinger")
        self.assertEqual(ct.name, "pinger")
        self.assertEqual(ct.desc, "Pinger")
        self.assertEqual(ct.contrib_desc, "Pinging")

    def _test_cannot_add(self, user):
        url = reverse("sources:ctype_add", kwargs={"sname": "test"})

        client = self.make_test_client(user)
        response = client.get(url)
        self.assertPermissionDenied(response)

        response = client.post(url)
        self.assertPermissionDenied(response)
        self.assertFalse(self.sources.test.contribution_types.filter(name="pinger").exists())

        # Post (valid data)
        response = client.post(url, data={"name": "pinger", "desc": "Pinger", "contrib_desc": "Pinging"})
        self.assertPermissionDenied(response)
        self.assertFalse(self.sources.test.contribution_types.filter(name="pinger").exists())

    def _test_can_update(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        url = reverse("sources:ctype_update", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)

        # Get (show form)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

        # Post (show form with validation errors)
        response = client.post(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())

        # Post (valid data)
        response = client.post(url, data={"name": "tester1", "desc": "Tester1", "contrib_desc": "Testing1"})
        self.assertRedirects(response, reverse("sources:update", args=["test"]))
        self.assertFalse(self.sources.test.contribution_types.filter(name="tester").exists())
        ct = self.sources.test.contribution_types.get(name="tester1")
        self.assertEqual(ct.name, "tester1")
        self.assertEqual(ct.desc, "Tester1")
        self.assertEqual(ct.contrib_desc, "Testing1")

    def _test_cannot_update(self, user):
        url = reverse("sources:ctype_update", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)
        response = client.get(url)
        self.assertPermissionDenied(response)

        response = client.post(url)
        self.assertPermissionDenied(response)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())

        # Post (valid data)
        response = client.post(url, data={"name": "tester1", "desc": "Tester1", "contrib_desc": "Testing1"})
        self.assertPermissionDenied(response)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())
        self.assertFalse(self.sources.test.contribution_types.filter(name="tester1").exists())
        ct = self.sources.test.contribution_types.get(name="tester")
        self.assertEqual(ct.name, "tester")
        self.assertEqual(ct.desc, "tester_desc")
        self.assertEqual(ct.contrib_desc, "tester_cdesc")

    def _test_can_delete(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        url = reverse("sources:ctype_delete", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)

        # Get (show confirmation form)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())

        # Post (confirm deletion)
        response = client.post(url)
        self.assertRedirects(response, reverse("sources:update", args=["test"]))
        self.assertFalse(self.sources.test.contribution_types.filter(name="tester").exists())

    def _test_cannot_delete(self, user, member=False):
        url = reverse("sources:ctype_delete", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)
        response = client.get(url)
        self.assertPermissionDenied(response)

        response = client.post(url)
        self.assertPermissionDenied(response)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())


class SourceCtypeDeleteContributionsTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        # Admins can delete contributions without being members
        cls._add_method(cls._test_can_delete_contributions, "admin")
        # DD and alioth can delete contributions only if members
        cls._add_method(cls._test_cannot_delete_contributions, "dd")
        cls._add_method(cls._test_can_delete_contributions, "dd", member=True)
        cls._add_method(cls._test_cannot_delete_contributions, "alioth")
        cls._add_method(cls._test_can_delete_contributions, "alioth", member=True)
        # Anonymous cannot delete contributions
        cls._add_method(cls._test_cannot_delete_contributions, None)

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.ctypes.create("pinger", source=cls.sources.test, name="pinger", desc="Pinger", contrib_desc="Pinging")
        dates = {
            "begin": datetime.date(2015, 2, 1),
            "until": datetime.date(2015, 2, 15),
        }
        cls.contributions.create(
                "pinger_dd_user", type=cls.ctypes.pinger, identifier=cls.idents.dd_user, **dates)
        cls.contributions.create(
                "pinger_alioth_user", type=cls.ctypes.pinger, identifier=cls.idents.alioth_user, **dates)
        cmodels.AggregatedPersonContribution.recompute(ctype=cls.ctypes.tester)
        cmodels.AggregatedPersonContribution.recompute(ctype=cls.ctypes.pinger)
        cmodels.AggregatedSource.recompute(source=cls.sources.test)

    def _test_can_delete_contributions(self, user, member=False):
        if member:
            self.sources["test"].admins.add(self.users[user])

        url = reverse("sources:ctype_delete_contributions", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)

        # Get (show confirmation form)
        response = client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.ctypes.tester.contributions.count(), 10)
        self.assertEqual(self.ctypes.pinger.contributions.count(), 2)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.tester).count(), 5)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.pinger).count(), 2)
        self.assertEqual(cmodels.AggregatedSource.objects.filter(source=self.sources.test).count(), 1)

        # Post (confirm deletion)
        response = client.post(url)
        self.assertRedirects(response, self.sources.test.get_absolute_url())
        self.assertEqual(self.ctypes.tester.contributions.count(), 0)
        self.assertEqual(self.ctypes.pinger.contributions.count(), 2)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.tester).count(), 0)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.pinger).count(), 2)
        self.assertEqual(cmodels.AggregatedSource.objects.filter(source=self.sources.test).count(), 1)

    def _test_cannot_delete_contributions(self, user, member=False):
        url = reverse("sources:ctype_delete_contributions", kwargs={"sname": "test", "name": "tester"})

        client = self.make_test_client(user)
        response = client.get(url)
        self.assertIn(response.status_code, [403, 405])  # Forbidden / Method not allowed

        response = client.post(url)
        self.assertPermissionDenied(response)
        self.assertTrue(self.sources.test.contribution_types.filter(name="tester").exists())

        self.assertEqual(self.ctypes.tester.contributions.count(), 10)
        self.assertEqual(self.ctypes.pinger.contributions.count(), 2)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.tester).count(), 5)
        self.assertEqual(cmodels.AggregatedPersonContribution.objects.filter(ctype=self.ctypes.pinger).count(), 2)
        self.assertEqual(cmodels.AggregatedSource.objects.filter(source=self.sources.test).count(), 1)
