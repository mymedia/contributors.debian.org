from django import http
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import DetailView, View
from contributors import models as cmodels
from django.utils.translation import ugettext as _
from dc.mixins import VisitorMixin
from signon.models import Identity


class ContributorMixin(VisitorMixin):
    def load_objects(self):
        super().load_objects()
        name = self.kwargs["name"]
        try:
            self.contributor = cmodels.User.objects.get(username=name)
        except cmodels.User.DoesNotExist:
            self.contributor = None

        if self.contributor is None:
            if name.endswith("@alioth"):
                identity = get_object_or_404(Identity, issuer="debsso", subject=f"{name[:-7]}@users.alioth.debian.org")
            elif name.endswith("@debian"):
                identity = get_object_or_404(Identity, issuer="debsso", subject=f"{name[:-7]}@debian.org")
            elif name.endswith("@salsa"):
                identity = get_object_or_404(Identity, issuer="salsa", username=f"{name[:-6]}")
            else:
                identity = None

            if identity is None or identity.person is None:
                identifier = cmodels.Identifier.objects.filter(name=name).first()

                if identifier is None or identifier.user is None:
                    raise http.Http404("No contributor found")
                return redirect(identifier.user, permanent=True)
            else:
                self.contributor = identity.person

        self.own_page = (
                self.contributor is not None
                and (self.request.user.is_superuser or self.request.user == self.contributor))

        # Someone else is visting this user: enforce visibility settings
        if self.contributor is not None and self.contributor.hidden and not self.own_page:
            self.contributor = None

        if self.contributor is None:
            raise http.Http404(_("No contributor found matching the query"))

    def check_visitor_permission(self, perm):
        if perm == "own_page":
            return self.own_page
        else:
            return super(ContributorMixin, self).check_visitor_permission(perm)

    def get_object(self):
        return self.contributor

    def get_context_data(self, **kw):
        ctx = super(ContributorMixin, self).get_context_data(**kw)
        ctx["contributor"] = self.contributor
        ctx["own_page"] = self.own_page
        return ctx


class ContributorView(ContributorMixin, DetailView):
    template_name = "contributor/contributor.html"
    context_object_name = 'contributor'

    def get_context_data(self, **kw):
        context = super().get_context_data(**kw)

        if self.own_page:
            context["user_log"] = list(self.object.log.order_by("-ts"))
            source_settings = cmodels.UserSourceSettings.objects.filter(user=self.object, hidden=True).distinct()
            identifiers = []
            for ident in self.object.identifiers.order_by("type", "name"):
                identifiers.append({
                    "ident": ident,
                    "log": ident.log.order_by("-ts"),
                })
            context["identifiers"] = identifiers
            context["source_settings"] = list(source_settings)
            context["hide_user"] = self.object.hidden

        contributions = []
        for c in self.object.contributions().order_by("identifier"):
            if self.own_page or not c.hidden:
                contributions.append(c)

        context["contributions"] = contributions
        context['redacted'] = not self.own_page
        return context


class SaveSettings(ContributorMixin, View):
    require_visitor = "own_page"

    def post(self, request, *args, **kw):
        changed = False

        # Updated user info
        full_name = self.request.POST.get("full_name", "")
        if full_name != self.contributor.full_name:
            self.contributor.full_name = full_name
            self.contributor.save()

        # Update user visibility
        want_hidden_user = bool(request.POST.get("hide_user", False))
        if self.contributor.hidden != want_hidden_user:
            self.contributor.hidden = want_hidden_user
            self.contributor.save()
            changed = True

        # Update identifier visibility
        for ident in self.contributor.identifiers.all():
            want_hidden = bool(self.request.POST.get("hide_{}".format(ident.pk), False))
            if ident.hidden != want_hidden:
                ident.hidden = want_hidden
                ident.save()
                changed = True

        if changed:
            cmodels.AggregatedPersonContribution.recompute(user=self.contributor)
            cmodels.AggregatedSource.recompute()
            cmodels.AggregatedPerson.recompute(user=self.contributor)

        return redirect(self.contributor.get_absolute_url())
